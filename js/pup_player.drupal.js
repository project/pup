/**
 * Adds player logic to the page.
 */
Drupal.behaviors.pupPlayer = function (context) {

  /**
   * Constructor for the Playlist object.
   *
   * @param options
   *   jPlayer constructor options for this playlist. 
   */
  var Playlist = function(options) {
    this.initCssSelectors();
    this.options = options; // jPlayer options.
    this.isPlaying = false;
    this.interval = null; // Interval for updating playlist based on dirty flag.
    this.initPlayer();
  };

  /**
   * Initializes CSS selectors for player components.
   */
  Playlist.prototype.initCssSelectors = function() {
    // Element ids for player components.
    this.cssId = {
      player: 'jp-player',
      interface: 'jp-interface',
      playlist: 'jp-playlist'
    };
    
    // Element selectors for player components (all derived from cssId).
    this.cssSelector = {};
    for (var property in this.cssId) {
      this.cssSelector[property] = '#' + this.cssId[property];
    }
  }
  
  /**
   * Initializes the player with its options.
   */
  Playlist.prototype.initPlayer = function() {
    var self = this;

    $.jPlayer.timeFormat.showHour = true; // TODO: We may not want to hardcode this.
    
    if (!this.options.cssSelectorAncestor) {
      this.options.cssSelectorAncestor = this.cssSelector.interface;
    }
    
    $(this.cssSelector.player).jPlayer(this.options);

    // Bind player buttons.
    $(this.cssSelector.interface + ' .jp-previous').click(function() {
      self.previous();
      $(this).blur();
      return false;
    });
    $(this.cssSelector.interface + ' .jp-next').click(function() {
      self.next();
      $(this).blur();
      return false;
    });
  }

  /**
   * Shows or hides the empty playlist message.
   *
   * @param status
   *   Whether to show or hide the message.
   */
  Playlist.prototype.showEmpty = function(status) {
    if (status) {
      $('.pup-playlist-empty').show();
    }
    else {
      $('.pup-playlist-empty').hide();
    }
  }
  
  /**
   * Starts updating playlist based on dirty flag.
   */
  Playlist.prototype.listenTrackAdditions = function() {
    var self = this;
    var update = function() {
      // Flush storage cache since it may have been changed from
      // outside the player.
      $.jStorage.reInit();

      // Check dirty flag.
      var dirtyTimestamp = $.jStorage.get('pup-dirty-timestamp', 0);
      if (dirtyTimestamp) {
        // Reset dirty flag.
        $.jStorage.deleteKey('pup-dirty-timestamp'); 

        // Retrieve the playlist.
        var playlist = $.jStorage.get('pup-playlist', new Array());
        var currentTimestamp = $.jStorage.get('pup-current-timestamp', 0);
        for (var i = 0; i < playlist.length; i++) {
          var track = playlist[i];
          if (track.timestamp >= dirtyTimestamp) {
            var isCurrent = track.timestamp == currentTimestamp;
            self.displayTrack(track, isCurrent);
            if (isCurrent) {
              $(self.cssSelector.player).jPlayer('setMedia', track);
            }
          }
        }

        // Ensure that a track is defined as the current one.
        self.ensureCurrentTrack(playlist, currentTimestamp);

        // Show/hide empty message.
        if (playlist.length) {
          self.showEmpty(false);
        }
        else {
          self.showEmpty(true);
        }
      }
    }
  
    if (!this.interval) {
      this.interval = setInterval(update, 500);
    }
  }

  /**
   * Formats a track CSS id.
   *
   * @param timestamp
   *   Timestamp of the track whose id is to be generated.
   */
  Playlist.prototype.makeTrackCssId = function(timestamp) {
    return this.cssId.playlist + '-track-' + timestamp;
  }
    
  /**
   * Formats a track CSS selector.
   *
   * @param timestamp
   *   Timestamp of the track whose id is to be generated.
   */
  Playlist.prototype.makeTrackCssSelector = function(timestamp) {
    return '#' + this.makeTrackCssId(timestamp);
  }

  /**
   * Appends a track to the playlist's markup.
   *
   * @param track
   *   The track to display.
   * @param isCurrent
   *   When true, show the track as the current one.
   */
  Playlist.prototype.displayTrack = function(track, isCurrent) {
    var self = this;
    var trackCssSelector = this.makeTrackCssSelector(track.timestamp);

    // Append track.
    $(this.cssSelector.playlist + ' ul').append(this.formatTrack(track, isCurrent));

    // Bind play handler.
    $(trackCssSelector).data('timestamp', track.timestamp).click(function() {
      self.changeCurrentTrack($(this).data('timestamp'), true);
      return false;
    });

    // Bind remove handler.
    $(trackCssSelector + ' ~ .pup-track-remove').data('timestamp', track.timestamp).click(function() {
      self.removeTrack($(this).data('timestamp'));
      $(this).blur();
      return false;
    });
  }
  
  /**
   * Formats a track's markup.
   *
   * @param track
   *   The track to format.
   * @param isCurrent
   *   When true, display the track as the current one.
   */
  Playlist.prototype.formatTrack = function(track, isCurrent) {
    var output = '<li' + (isCurrent ? ' class="jp-playlist-current"' : '') + '>';
    output += '<a href="#" id="' + this.makeTrackCssId(track.timestamp) +'" tabindex="1" title="' + Drupal.t('Play this track') + '" class="pup-track-title' + (isCurrent ? ' jp-playlist-current' : '') + '">' + track.name + '</a>';
    output += '<a class="pup-track-remove" href="#" tabindex="1" title="' + Drupal.t('Remove this track') + '">×</a>';
    output += '</li>';
    return output;
  }

  /**
   * Ensures that a track is defined as the current one.
   */
  Playlist.prototype.ensureCurrentTrack = function(playlist, currentTimestamp) {
    if (!currentTimestamp && playlist.length) {
      // Select the first track as the current one.
      $(this.makeTrackCssSelector(playlist[0].timestamp)).addClass('jp-playlist-current').parent().addClass('jp-playlist-current');
      $.jStorage.set('pup-current-timestamp', playlist[0].timestamp);
      $(this.cssSelector.player).jPlayer('setMedia', playlist[0]);
    }
  }

  /**
   * Switches current track to another one.
   *
   * @param timestamp
   *   Timetamp of the track to make current.
   */
  Playlist.prototype.changeCurrentTrack = function(timestamp, forcePlay) {
    var self = this;
    var currentTimestamp = $.jStorage.get('pup-current-timestamp', 0);
    if (currentTimestamp != timestamp) {
      var playlist = $.jStorage.get('pup-playlist', new Array());
      // Find the track to set as current.
      $.each(playlist, function(index, track) {
        if (track.timestamp == timestamp) {
          // Switch CSS classes.
          $(self.cssSelector.playlist + ' ul .jp-playlist-current').removeClass('jp-playlist-current');
          $(self.makeTrackCssSelector(timestamp)).addClass('jp-playlist-current').parent().addClass('jp-playlist-current');
          // Save new current timestamp.
          $.jStorage.set('pup-current-timestamp', timestamp);
          // Switch player track.
          $(self.cssSelector.player).jPlayer('setMedia', track);
          // Continue playing.
          if (self.isPlaying || forcePlay) {
            self.play();
          }
          return false; // Break loop.
        }
      });
    }
    else if (!this.isPlaying) {
      this.play();
    }
  }

  
  /**
   * Plays the current track.
   */
  Playlist.prototype.play = function() {
    $(this.cssSelector.player).jPlayer('play');
  }
  
  /**
   * Stops playing the current track.
   */
  Playlist.prototype.stop = function() {
    $(this.cssSelector.player).jPlayer('stop');
  }
  
  /**
   * Set current track to the next one.
   */
  Playlist.prototype.next = function() {
    var playlist = $.jStorage.get('pup-playlist', new Array());
    var currentTimestamp = $.jStorage.get('pup-current-timestamp', 0);
    for (var i = 0; i < playlist.length; i++) {
      if (playlist[i].timestamp == currentTimestamp) {
        if (i < playlist.length - 1) {
          this.changeCurrentTrack(playlist[i + 1].timestamp);
        }
        else if (i != 0) {
          this.changeCurrentTrack(playlist[0].timestamp);
        }
      }
    }
  }

  /**
   * Set current track to the previous one.
   */
  Playlist.prototype.previous = function() {
    var playlist = $.jStorage.get('pup-playlist', new Array());
    var currentTimestamp = $.jStorage.get('pup-current-timestamp', 0);
    for (var i = 0; i < playlist.length; i++) {
      if (playlist[i].timestamp == currentTimestamp) {
        if (i > 0) {
          this.changeCurrentTrack(playlist[i - 1].timestamp);
        }
        else if (i != playlist.length - 1) {
          this.changeCurrentTrack(playlist[playlist.length - 1].timestamp);
        }
      }
    }
  }

  /**
   * Removes a track both from storage and the display.
   */
  Playlist.prototype.removeTrack = function(timestamp) {
    var self = this;
    var playlist = $.jStorage.get('pup-playlist', new Array());
    var currentTimestamp = $.jStorage.get('pup-current-timestamp', 0);
    $.each(playlist, function(index, track) {
      if (track.timestamp == timestamp) {
        if (timestamp == currentTimestamp) {
          self.next();
        }
        // Remove from display.
        var trackCssSelector = self.makeTrackCssSelector(timestamp);
        $(trackCssSelector).parent().remove();
        // Remove from storage.
        playlist.splice(index, 1);
        $.jStorage.set('pup-playlist', playlist);
        // Set dirty flag.
        $.jStorage.set('pup-dirty-removal', true);
        return false; // Break loop.
      }
    });
    if (playlist.length == 0) {
      this.stop();
      this.showEmpty(true);
      $.jStorage.deleteKey('pup-current-timestamp');
      $(self.cssSelector.player).jPlayer('setMedia', null);
    }
  }
  
  // Instanciate playlist.
  var audioPlaylist = new Playlist({
    ready: function() {
      audioPlaylist.listenTrackAdditions();
      $.jStorage.set('pup-dirty-timestamp', 1); // Force refresh.
    },
    ended: function() {
      audioPlaylist.next();
    },
    play: function() {
      audioPlaylist.isPlaying = true;
      $(this).jPlayer('pauseOthers');
    },
    stop: function() {
      audioPlaylist.isPlaying = false;
    },
    pause: function() {
      audioPlaylist.isPlaying = false;
    },
    swfPath: Drupal.settings.pup.swfPath,
    supplied: 'mp3',
    cssSelector: { videoPlay: '' } // Override default selector. No need for video playback area.
  });
}
