README file for the Pop-Up Player (PUP) Drupal module.


-- DESCRIPTION --

The Pop-Up Player (PUP) module allows users to add audio files to a
playlist as they surf the site. PUP also provides an audio player in a
separate window from the browser's main window, allowing users to
listen to their playlist, uninterrupted, while navigating the site.

Some highlights:

* The playlist is saved in the browser's local storage. Nothing gets
  saved in Drupal.
  
* No user account on the site is required to create playlists.

* You may provide "Add to playlist" links in your pages just by
  wrapping any link pointing to an audio file with specific markup
  (see Installation below).
  
* The player is based on HTML5 and JavaScript, with a fallback to
  Flash. Playback is handled by jPlayer (http://www.jplayer.org).

Sites using PUP:

* http://radiospirale.org/ (in French only; try the "(+) Ajouter à la
  liste d'écoute" and "Ouvrir le lecteur audio" links).
  

-- REQUIREMENTS --

* Drupal 6.x
* Drupal Module: jQuery Update (http://drupal.org/project/jquery_update)
* Drupal Module: Libraries (http://drupal.org/project/libraries)
* Library: jPlayer (http://www.jplayer.org). Tested with 2.0.0.
* Library: jStorage (http://www.jstorage.info)
* Library: jQuery Color (https://github.com/jquery/jquery-color)


-- LIMITATIONS --

* Internet Explorer 6 and Internet Explorer 7 are not supported
  because of restrictions with userData Behavior, which has a "same
  path" (rather than "same domain") access limitation. [ISSUE# HERE]

* Although jPlayer supports other formats, at the moment, only MP3
  files are supported. [ISSUE# HERE]

* MP3 files used must be encoded according to the browser's Adobe
  Flash Plugin limitations:

  Constant Bitrate Encoded.
  Sample Rate a multiple of 11,025Hz. ie., 22,050Hz and 44,100Hz are
  valid sample rates.

* Your server must be configured properly to serve media files. See
  jPlayer's documentation:
  
  http://www.jplayer.org/latest/developer-guide/#jPlayer-server-response

* The player window won't show up at all if JavaScript is disabled.


-- INSTALLATION --

* Download and extract the above required libraries to your site's
  library directory. If your library directory is sites/all/libraries,
  PUP expects to find:

  sites/all/libraries/jplayer
  sites/all/libraries/jstorage
  sites/all/libraries/jquery-color

* Install the jQuery Update and the Libraries modules (follow
  instructions for each). 

* Install the PUP module as usual. See http://drupal.org/node/70151
  for more information.

* Modify the output of your links to audio files by wrapping the links
  with an element having:

  - the "pup-playlist-add" class.
  - a title attribute containing the title to show in the playlist.

  Here's an example link that will be picked up by PUP:

  <span class="pup-playlist-add" title="The Drupal Song"><a href="http://example.com/sites/default/files/drupalsong.mp3">Add to playlist</a></span>

  The way to modify those links will depend on what module generates
  them. It likely involves implementing a theme function in your
  theme. If the links are shown through Views, you may be able to
  rewrite those links within the field's configuration in Views.
  
* Go to Administer > User management > Permissions, and assign the
  "access popup player" to proper roles.

* Go to Administer > Site building > Blocks, and assign the "Pop-Up
  Player opener" block to an appropriate region.
  

-- MISCELLANEOUS NOTES --
  
Keys used in local storage:
* pup-dirty-timestamp: Timestamp of the last added track.
* pup-playlist: The playlist.
* pup-current-timestamp: Timestamp of the current track.


-- SUPPORT --

For support requests, bug reports, and feature requests, please use
PUP's issue queue on drupal.org [LINK HERE].

Please DO NOT send bug reports through e-mail or personal contact
forms, use the aforementioned issue queue instead.


-- CREDITS --

Current maintainer:
* David Lesieur (http://drupal.org/user/17157)

Skins:
* Blue Monday: based on the Blue Monday skin provided with jPlayer.
* Bloody Sunday: by Laboratoire NT2 (derived from Blue Monday).

This project has been sponsored by:
* Laboratoire NT2 (http://nt2.uqam.ca)
* Whisky Echo Bravo (http://whiskyechobravo.com)

